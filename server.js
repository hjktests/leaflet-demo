const express = require('express');
const path = require('path');
const request = require('request');

const app = express();

const PORT = process.env.PORT || 80;

// Use build dir
app.use(express.static(path.join(__dirname, 'build')));

// Allow content from external origins
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});


app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'))
});

app.use('/api', require('./src/routes/api'));

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));