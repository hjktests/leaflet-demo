import React from 'react';
// import './App.css';
import Map from './components/Map.js';
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div id="App">
        <Route exact path='/' component={Map}/>
      </div>
    </Router>
  );
}

export default App;
