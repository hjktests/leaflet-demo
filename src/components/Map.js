import React from 'react';
import ReactDOM from 'react-dom';
import L from 'leaflet';
import axios from 'axios';
class Map extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            map: null,
            location: null
        };
        this.map = null;
        this.location = null;
    }


    componentDidMount() {
        while(!this.map){
            this.initMap(this.getUserLocation);
        }
        this.testPlace();
    }



    testPlace = () => {

        axios.get('/api')
            .then(response => {
                // If request is good
                console.log(response.data.location.coordinates.wgs84.lat);
                console.log(response.data.location.coordinates.wgs84.lon);
                L.marker([response.data.location.coordinates.wgs84.lat,response.data.location.coordinates.wgs84.lon]).addTo(this.map);
            })
            .catch((error) => {
                console.log('TestPlace ' + error);
            });

    }


    initMap = (callback) => {
        // console.log('Init map')
        // Get current element
        // let element = ReactDOM.findDOMNode(this);

        // console.log(this.map)

        this.map = L.map('mapScreen', {
            center: new L.LatLng(62.241587210186474, 25.749464),
            zoom: 13,
            maxZoom: 18,
            zoomControl: true,
            layers: new L.TileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 20,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            })
        }, 100)

        // console.log(this.map)
        // Move onto next function
        callback();
    }


    getUserLocation = () => {
        // console.log('Get user location -->')
        navigator.geolocation.getCurrentPosition(position => {
            // console.log(position.coords)
            this.location = [position.coords.latitude, position.coords.longitude, position.coords.accuracy];
            console.log(this.location)
            this.setState({
                location: [position.coords.latitude, position.coords.longitude, position.coords.accuracy]
            });
        }, error => console.log(error.message), {
            enableHighAccuracy: true
        });

    }


    render() {
        if (this.state.location) {
            L.marker([this.state.location[0], this.state.location[1]]).addTo(this.map).bindPopup(`Olet ${this.state.location[2]}m säteellä täältä`).openPopup();
            this.map.setView([this.state.location[0], this.state.location[1]], 15);
        }
        

        return ( <div id = "mapScreen" > </div>
        )
    }
}

export default Map;