const express = require('express');
const router = express.Router();
const axios = require('axios');

router.get('/', (req, res) => {
    axios.get('http://lipas.cc.jyu.fi/api/sports-places/75175')
    .then( (result) => {
        res.send(result.data)
    })
})

router.get('/:lat/:lon', (req, res) => {
    var lat = req.params.lat,
    lon = req.params.lon;

    axios.get(`http://lipas.cc.jyu.fi/api/sports-places?closeToLon=${lon}&closeToLat=${lat}&closeToDistanceKm=100`)
    .then( (result) => {
        res.send(result.data)
    })
});


module.exports = router;